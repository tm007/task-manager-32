package ru.tsc.apozdnov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        serviceLocator.getAuthEndpoint().connect();
        @Nullable final Socket socket = serviceLocator.getAuthEndpoint().getSocket();
        serviceLocator.getSystemEndpoint().setSocket(socket);
        serviceLocator.getDomainEndpoint().setSocket(socket);
        serviceLocator.getProjectEndpoint().setSocket(socket);
        serviceLocator.getProjectTaskEndpoint().setSocket(socket);
        serviceLocator.getTaskEndpoint().setSocket(socket);
        serviceLocator.getUserEndpoint().setSocket(socket);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}