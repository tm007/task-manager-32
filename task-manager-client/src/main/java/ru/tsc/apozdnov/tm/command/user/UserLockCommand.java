package ru.tsc.apozdnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.UserLockRequest;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        getUserEndpoint().lockUser(new UserLockRequest(login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}