package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    ProjectTaskEndpointClient getProjectTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

}