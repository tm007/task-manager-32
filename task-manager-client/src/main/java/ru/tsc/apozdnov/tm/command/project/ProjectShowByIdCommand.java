package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.request.ProjectShowByIdRequest;
import ru.tsc.apozdnov.tm.dto.response.ProjectShowByIdResponse;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(id);
        @NotNull final ProjectShowByIdResponse response = getProjectEndpoint().showProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}