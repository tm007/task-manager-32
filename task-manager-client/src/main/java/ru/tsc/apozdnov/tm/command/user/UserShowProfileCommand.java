package ru.tsc.apozdnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.request.UserProfileRequest;
import ru.tsc.apozdnov.tm.dto.response.UserProfileResponse;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.exception.entity.UserNotFoundException;
import ru.tsc.apozdnov.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull UserProfileResponse response = getUserEndpoint().showProfileUser(new UserProfileRequest());
        @Nullable final User user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}