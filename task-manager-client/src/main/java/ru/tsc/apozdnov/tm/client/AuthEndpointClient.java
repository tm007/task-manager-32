package ru.tsc.apozdnov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.apozdnov.tm.api.endpoint.IEndpointClient;
import ru.tsc.apozdnov.tm.dto.request.UserLoginRequest;
import ru.tsc.apozdnov.tm.dto.request.UserLogoutRequest;
import ru.tsc.apozdnov.tm.dto.request.UserProfileRequest;
import ru.tsc.apozdnov.tm.dto.response.UserLoginResponse;
import ru.tsc.apozdnov.tm.dto.response.UserLogoutResponse;
import ru.tsc.apozdnov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint, IEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}