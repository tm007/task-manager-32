package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.apozdnov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(id);
        @NotNull ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

}