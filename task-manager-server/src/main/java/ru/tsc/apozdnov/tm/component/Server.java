package ru.tsc.apozdnov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.endpoint.Operation;
import ru.tsc.apozdnov.tm.dto.request.AbstractRequest;
import ru.tsc.apozdnov.tm.dto.response.AbstractResponse;
import ru.tsc.apozdnov.tm.task.AbstractServerTask;
import ru.tsc.apozdnov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @Getter
    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final Integer port = bootstrap.getPropertyService().getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @Nullable
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

}