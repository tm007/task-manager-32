package ru.tsc.apozdnov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}