package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.service.IAuthService;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.exception.field.EmptyLoginException;
import ru.tsc.apozdnov.tm.exception.field.EmptyPasswordException;
import ru.tsc.apozdnov.tm.exception.system.IncorrectLoginOrPasswordException;
import ru.tsc.apozdnov.tm.exception.system.LockedUserException;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new LockedUserException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return user;
    }

}