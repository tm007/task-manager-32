package ru.tsc.apozdnov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.service.IServiceLocator;
import ru.tsc.apozdnov.tm.dto.request.AbstractUserRequest;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.exception.system.AccessDeniedException;
import ru.tsc.apozdnov.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(AbstractUserRequest request, Role role) {
        @NotNull final User user = checkUser(request);
        if (role == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        if (userRole != role) throw new AccessDeniedException();
    }

    protected void check(AbstractUserRequest request) {
        checkUser(request);
    }

    @NotNull
    private User checkUser(AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}