package ru.tsc.apozdnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.apozdnov.tm.api.service.IServiceLocator;
import ru.tsc.apozdnov.tm.dto.request.*;
import ru.tsc.apozdnov.tm.dto.response.*;
import ru.tsc.apozdnov.tm.enumerated.Role;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlLoadJaxbResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlSaveJaxbResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}