package ru.tsc.apozdnov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.Task;

public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable final Task task) {
        super(task);
    }

}