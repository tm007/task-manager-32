package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.Project;

@Getter
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

}