package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    private String userId;

}