package ru.tsc.apozdnov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final Integer index, @Nullable final Status status) {
        this.index = index;
        this.status = status;
    }

}