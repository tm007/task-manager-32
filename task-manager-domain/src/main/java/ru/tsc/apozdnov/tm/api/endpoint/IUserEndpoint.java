package ru.tsc.apozdnov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.*;
import ru.tsc.apozdnov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUpdateResponse updateUser(@NotNull UserUpdateRequest request);

    @NotNull
    UserProfileResponse showProfileUser(@NotNull UserProfileRequest request);

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

}