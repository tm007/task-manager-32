package ru.tsc.apozdnov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskShowByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}