package ru.tsc.apozdnov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.*;
import ru.tsc.apozdnov.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxbRequest request);

    @NotNull
    DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxbRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxbRequest request);

    @NotNull
    DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxbRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}
