package ru.tsc.apozdnov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable final String id, @Nullable final Status status) {
        this.id = id;
        this.status = status;
    }

}