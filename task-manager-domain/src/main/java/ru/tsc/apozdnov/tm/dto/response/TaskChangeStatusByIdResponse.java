package ru.tsc.apozdnov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.Task;

public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(@Nullable final Task task) {
        super(task);
    }

}