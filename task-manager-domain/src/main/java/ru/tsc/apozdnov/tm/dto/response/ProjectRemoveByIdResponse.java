package ru.tsc.apozdnov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.Project;

public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable final Project project) {
        super(project);
    }

}