package ru.tsc.apozdnov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.apozdnov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.apozdnov.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.apozdnov.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}